<<<<<<< HEAD
package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {
	
	private ArrayList<Object> shapes = new ArrayList<Object>();
	
	public double calculateTotalArea(){
		double totalArea = 0;

		for (Object obj : shapes){
			
			if(obj instanceof Circle)
			totalArea += ((Circle) obj).area(); 
			else if(obj instanceof Rectangle)
			totalArea += ((Rectangle) obj).area(); 
			else if(obj instanceof Square)
				totalArea += ((Square) obj).area(); 
		}
		return totalArea;
		
		
	}

	public void addShape(Object o) {
		shapes.add(o);
	}
	
	
}
=======
package drawing.version3;


import java.util.ArrayList;
import shapes.Circle;
import shapes.Rectangle;
import shapes.Shape;

public class Drawing {
	
	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	
	
	public double calculateTotalArea(){
		double totalArea = 0;

		for (Shape shape : shapes){
			totalArea += shape.area();    
		}	
		return totalArea;
	}
	
	public void addShape(Shape s) {
		shapes.add(s);
	}
	
	
}
>>>>>>> 64f5f60b0fda714954f960a5b9b9f4a40fededeb
